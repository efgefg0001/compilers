#!/usr/bin/env python3

from fa_algs import build_nfa
from draw import draw_nfa

if __name__ == "__main__":
#    inp = " bc"
#    inp = "ac|be"
#    inp = "((ad)|bc)*"
#    inp = "aaa|aaa|bb|dd|eee"
    inp = "(a|b)*abb"
#    inp = "(0|1)*(000|111)(0|1)*"
    nfa = build_nfa(inp)
    draw_nfa(nfa, "build/nfa")

#    dfa = nfa_to_dfa(nfa)

