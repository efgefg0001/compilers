#!/usr/bin/env python3

from parse import *
from dfa import *

__all__ = ["build_nfa", "nfa_to_dfa", "minimize_dfa", "match"]


def build_nfa(re_src):
    tokens = get_tokens(re_src)
    handler = Handler()
    nfa_stack = []
    for t in tokens:
        handler.handlers[t.name](t, nfa_stack)
    return nfa_stack.pop()


def nfa_to_dfa(nfa):
    converter = NfaToDfaConverter(nfa)
    return converter.convert()

def minimize_dfa(dfa):
    minimizer = DfaMinimizer(dfa)
    return minimizer.minimize()

def match(input_fa, string):
    matcher = Matcher(input_fa)
    return matcher.match(string)
