#!/usr/bin/env python3

import graphviz as gv
from nfa import Nfa

def draw_nfa(nfa: Nfa, filename):
    dg = gv.Digraph(format="svg")
    dg.attr("node",  shape="diamond")
    dg.node(nfa.states[nfa.start].data)
    dg.attr("node", shape="doublecircle")
    for f in nfa.finish:
        dg.node(nfa.states[f].data)

    dg.attr("node",  shape="circle")
    for trans in nfa.transitions.values():
        for tr in trans:
            if tr.is_empty():
                label = " emp   "
            else:
                label = " '%s'   " % tr.symbol
            dg.edge(tr.from_s, tr.to_s, label=label)

    dg.render(filename=filename)
