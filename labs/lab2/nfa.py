#!/usr/bin/env python3

class Nfa:
    def __init__(self):
        self.transitions = {}
        self.states = {}
        self.start = ""
        self.finish = []
        self.alphabet = set()

    def add_state(self, state):
        self.states[state.id] = state

    def add_trans(self, trans):
        from_s = trans.from_s
        if from_s not in self.transitions:
            self.transitions[from_s] = [trans]
        else:
            self.transitions[from_s].append(trans)

    def add_to_finish(self, st_id):
        if st_id not in self.finish:
            self.finish.append(st_id)

    def add_to_alphabet(self, symbol):
        if symbol not in self.alphabet: # можно этого не делать
            self.alphabet.add(symbol)


class State:
    def __init__(self, id, data):
        self.id = id
        self.data = data

    def __str__(self):
        return str(self.__dict__)


class Trans:
    def __init__(self, from_s, to_s, symbol=None):
        self.from_s = from_s
        self.to_s = to_s
        self.symbol = symbol

    def is_empty(self):
        return self.symbol is None

    def __str__(self):
        return str(self.__dict__)

    def equals(self, tr):
        return self.from_s == tr.from_s and self.to_s == tr.to_s and self.symbol == tr.symbol
