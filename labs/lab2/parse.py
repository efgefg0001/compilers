#!/usr/bin/env python3

import random
from nfa import *

class Token:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __str__(self):
        return self.name + ":" + self.value

class TokenName:
    LEFT_PAREN = "LEFT_PAREN",
    RIGHT_PAREN = "RIGHT_PAREN"
    STAR = "STAR"
    ALT = "ALT"
    CONCAT = "CONCAT"
    PLUS = "PLUS"
    QMARK = "QMARK"
    CHAR = "CHAR"
    NONE = "NONE"
    EMPTY = "EMPTY"

class Lexer:
    def __init__(self, pattern):
        self.source = pattern
        self.symbols = {
            '(':TokenName.LEFT_PAREN, ')':TokenName.RIGHT_PAREN, '*':TokenName.STAR, '|':TokenName.ALT,
            '\x08':TokenName.CONCAT, '+':TokenName.PLUS, '?':TokenName.QMARK}
        self.current = 0
        self.length = len(self.source)
       
    def get_token(self): 
        if self.current < self.length:
            c = self.source[self.current]
            self.current += 1
            if c not in self.symbols.keys(): # CHAR
                token = Token(TokenName.CHAR, c)
            else:
                token = Token(self.symbols[c], c)
            return token
        else:
            return Token(TokenName.NONE, '')

class ParseError(Exception):pass

class Parser:

    def __init__(self, lexer):
        self.lexer = lexer
        self.tokens = []
        self.lookahead = self.lexer.get_token()
    
    def consume(self, name):
        if self.lookahead.name == name:
            self.lookahead = self.lexer.get_token()
        elif self.lookahead.name != name:
            raise ParseError

    def parse(self):
        if self.lexer.length == 0:
            return [Token(TokenName.EMPTY, '')]

        self.exp()
        return self.tokens
    
    def exp(self):
        self.term()
        if self.lookahead.name == TokenName.ALT:
            t = self.lookahead
            self.consume(TokenName.ALT)
            self.exp()
            self.tokens.append(t)

    def term(self):
        self.factor()
        if self.lookahead.value not in ')|':
            self.term()
            self.tokens.append(Token(TokenName.CONCAT, '\x08'))
    
    def factor(self):
        self.primary()
        if self.lookahead.name in {TokenName.STAR, TokenName.PLUS, TokenName.QMARK}:
            self.tokens.append(self.lookahead)
            self.consume(self.lookahead.name)

    def primary(self):
        if self.lookahead.name == TokenName.LEFT_PAREN:
            self.consume(TokenName.LEFT_PAREN)
            self.exp()
            self.consume(TokenName.RIGHT_PAREN)
        elif self.lookahead.name == TokenName.CHAR:
            self.tokens.append(self.lookahead)
            self.consume(TokenName.CHAR)

class Handler:
    def __init__(self):
        self.handlers = {TokenName.CHAR:self.handle_char, TokenName.CONCAT:self.handle_concat,
                         TokenName.ALT:self.handle_alt, TokenName.STAR:self.handle_rep,
                         TokenName.EMPTY: self.handle_empty}
        self.pool = set()

    def handle_empty(self, t, nfa_stack):
        nfa = Nfa()
        s_b = self.create_state()
        s_e = self.create_state()
        nfa.add_state(s_b)
        nfa.add_state(s_e)
        nfa.start = s_b.id
        nfa.add_to_finish(s_e.id)
        nfa.add_trans(Trans(s_b.id, s_e.id))

        nfa_stack.append(nfa)


    def create_state(self):
        max_num = 1000
        num = random.randrange(max_num)
        while num in self.pool:
            num = random.randrange(max_num)
        self.pool.add(num)

        msg = "s-{}".format(num)
        state = State(msg, msg)
        return state

    def handle_char(self, t, nfa_stack):
        state_one = self.create_state()
        state_two = self.create_state()
        trans = Trans(state_one.id, state_two.id, t.value)
        nfa = self.__construct_nfa(state_one, state_two, trans)

        # add to alphabet
        nfa.add_to_alphabet(t.value)

        nfa_stack.append(nfa)

    def __construct_nfa(self, state_one, state_two, trans):
        nfa = Nfa()
        nfa.add_state(state_one)
        nfa.add_state(state_two)
        nfa.add_trans(trans)
        nfa.start = trans.from_s
        nfa.add_to_finish(trans.to_s)
        return nfa
    
    def handle_concat(self, t, nfa_stack):
        n2: Nfa = nfa_stack.pop()
        n1: Nfa = nfa_stack.pop()

        for it in n1.finish:
            if it not in n1.transitions:
                n1.transitions[it] = []
            if n2.start in n2.transitions:
                for tr in n2.transitions[n2.start]:
                    n1.add_trans(Trans(it, tr.to_s, tr.symbol))
                del n2.states[n2.start]
                del n2.transitions[n2.start]

        for (k, v) in n2.states.items():
            n1.states[k] = v

        for (k, v) in n2.transitions.items():
            n1.transitions[k] = v
        n1.finish = n2.finish

        # copy alphabet
        for symb in n2.alphabet:
            n1.add_to_alphabet(symb)

        nfa_stack.append(n1)
    
    def handle_alt(self, t, nfa_stack):
        n2: Nfa = nfa_stack.pop()
        n1: Nfa = nfa_stack.pop()

        s0 = self.create_state()
        new_nfa = Nfa()
        new_nfa.start = s0.id
        new_nfa.add_state(s0)
        new_nfa.add_trans(Trans(s0.id, n1.start))
        new_nfa.add_trans(Trans(s0.id, n2.start))

        s_f = self.create_state()

        for it in n1.finish:
            n1.add_trans(Trans(it, s_f.id))

        for it in n2.finish:
            n2.add_trans(Trans(it, s_f.id))

        for s in n1.states.values():
            new_nfa.add_state(s)
        for t in n1.transitions.values():
            for tr in t:
                new_nfa.add_trans(tr)

        for s in n2.states.values():
            new_nfa.add_state(s)
        for t in n2.transitions.values():
            for tr in t:
                new_nfa.add_trans(tr)

        new_nfa.add_state(s_f)
        new_nfa.add_to_finish( s_f.id)

        # copy alphabet
        for symb in n1.alphabet | n2.alphabet:
            new_nfa.add_to_alphabet(symb)

        nfa_stack.append(new_nfa)
    
    def handle_rep(self, t, nfa_stack):
        n1: Nfa = nfa_stack.pop()
        s_b = self.create_state()
        s_e = self.create_state()
        new_nfa = Nfa()
        new_nfa.start = s_b.id
        new_nfa.add_state(s_b)
        new_nfa.add_state(s_e)
        new_nfa.add_to_finish(s_e.id)

        new_nfa.add_trans(Trans(s_b.id, n1.start))
        new_nfa.add_trans(Trans(s_b.id, s_e.id))
        for f in n1.finish:
            n1.add_trans(Trans(f, n1.start))
            n1.add_trans(Trans(f, s_e.id))

        for s in n1.states.values():
            new_nfa.add_state(s)
        for t in n1.transitions.values():
            for tr in t:
                new_nfa.add_trans(tr)

        # copy alphabet
        for symb in n1.alphabet:
            new_nfa.add_to_alphabet(symb)

        nfa_stack.append(new_nfa)

def get_tokens(re_src):
    lexer = Lexer(re_src)
    parser = Parser(lexer)
    return parser.parse()
