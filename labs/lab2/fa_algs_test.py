#!/usr/bin/env python3

import unittest
from fa_algs import *
from draw import draw_nfa
from nfa import *


class BuildNfaTest(unittest.TestCase):
    def test_build_nfa_ok(self):
#        inp = " bc"
        inp = "ac|be"
    #    inp = "((ad)|bc)*"
    #    inp = "aaa|aaa|bb|dd|eee"
    #    inp = "(a|b)*abb"
    #    inp = "(0|1)*(000|111)(0|1)*"
        nfa = build_nfa(inp)
        draw_nfa(nfa, "build/nfa")
        self.assertIsNotNone(nfa)


class NfaToDfaTest(unittest.TestCase):
    def __create_nfa(self):
        nfa = Nfa()
        s_b = State("s-1", "s-1")
        nfa.add_state(s_b)
        s_m = State("s-m", "s-m")
        nfa.add_state(s_m)
        s_e = State("s-2", "s-2")
        nfa.add_state(s_e)
        nfa.start = s_b.id
        nfa.add_to_alphabet("a")
        nfa.add_to_alphabet("b")
        nfa.add_to_finish(s_e.id)
        nfa.add_trans(Trans(from_s=s_b.id, to_s=s_e.id, symbol="a"))
        nfa.add_trans(Trans(from_s=s_b.id, to_s=s_m.id))
        nfa.add_trans(Trans(from_s=s_m.id, to_s=s_e.id, symbol="b"))
        return nfa

    def create_nfa_from_book(self):
        nfa = Nfa()
        nfa.states = {
            "0": State("0", "0"),
            "1": State("1", "1"),
            "2": State("2", "2"),
            "3": State("3", "3"),
            "4": State("4", "4"),
            "5": State("5", "5"),
            "6": State("6", "6"),
            "7": State("7", "7"),
            "8": State("8", "8"),
            "9": State("9", "9"),
            "10": State("10", "10")
        }
        nfa.alphabet = {"a", "b"}
        nfa.transitions = {
            "0": [Trans("0", "7"), Trans("0", "1")],
            "1": [Trans("1", "2"), Trans("1", "4")],
            "2": [Trans("2", "3", "a")],
            "3": [Trans("3", "6")],
            "4": [Trans("4", "5", "b")],
            "5": [Trans("5", "6")],
            "6": [Trans("6", "1"), Trans("6", "7")],
            "7": [Trans("7", "8", "a")],
            "8": [Trans("8", "9", "b")],
            "9": [Trans("9", "10", "b")]
        }
        nfa.start = "0"
        nfa.finish = ["10"]
        return nfa

    def test_nfa_to_dfa_ok(self):
        #        nfa = self.__create_nfa()
        nfa = self.create_nfa_from_book()
        try:
            draw_nfa(nfa, "build/nfa-to-dfa")
        except Exception as err:
            msg = str(err)
        inp = "ac|be"
    #    inp = "((ad)|bc)*"
    #    inp = "aaa|aaa|bb|dd|eee"
    #    inp = "(a|b)*abb"
    #    inp = "(0|1)*(000|111)(0|1)*"
        nfa = build_nfa(inp)
        dfa = nfa_to_dfa(nfa)
        draw_nfa(dfa, "build/dfa")
        self.assertIsNotNone(dfa)


class MinimizeDfaTest(unittest.TestCase):
    def create_dfa(self):
        dfa = Nfa()
        dfa.alphabet = {"0", "1"}
        dfa.states = {
            "A": State("A", "A"),
            "B": State("B", "B"),
            "C": State("C", "C"),
            "D": State("D", "D"),
            "E": State("E", "E"),
            "F": State("F", "F"),
            "G": State("G", "G"),
            "H": State("H", "H"),
        }
        dfa.transitions = {
            "A": [Trans("A", "H", "0"), Trans("A", "B", "1")],
            "B": [Trans("B", "A", "1"), Trans("B", "H", "0")],
            "C": [Trans("C", "E", "0"), Trans("C", "F", "1")],
            "D": [Trans("D", "E", "0"), Trans("D", "F", "1")],
            "E": [Trans("E", "F", "0"), Trans("E", "G", "1")],
            "F": [Trans("F", "F", "0"), Trans("F", "F", "1")],
            "G": [Trans("G", "G", "0"), Trans("G", "F", "1")],
            "H": [Trans("H", "C", "0"), Trans("H", "C", "1")],
        }
        dfa.start = "A"
        dfa.finish = ["G", "F"]
        return dfa

    def test_minimize_dfa_ok(self):
        input_dfa = self.create_dfa()
        try:
            draw_nfa(input_dfa, "build/input_dfa")
        except Exception as err:
            msg = str(err)
        inp = "ac|be"
    #    inp = "((ad)|bc)*"
    #    inp = "aaa|aaa|bb|dd|eee"
    #    inp = "(a|b)*abb"
    #    inp = "(0|1)*(000|111)(0|1)*"
        nfa = build_nfa(inp)
        dfa = nfa_to_dfa(nfa)
        min_dfa = minimize_dfa(dfa)

        try:
            draw_nfa(min_dfa, "build/output_dfa")
        except Exception as err:
            msg = str(err)

        self.assertIsNotNone(min_dfa)


class MatcherTest(unittest.TestCase):
    def create_dfa(self):
        dfa = Nfa()
        dfa.alphabet = {"0", "1"}
        dfa.states = {
            "A": State("A", "A"),
            "B": State("B", "B"),
            "C": State("C", "C"),
        }
        dfa.transitions = {
            "A": [Trans("A", "B", "0")],
            "B": [Trans("B", "C", "1")],
        }
        dfa.start = "A"
        dfa.finish = ["C"]
        return dfa

    def test_match_True(self):
        input_dfa = self.create_dfa()
        try:
            draw_nfa(input_dfa, "build/input_dfa")
        except Exception as err:
            msg = str(err)
        string = "a"

        inp = "ac|be"
    #    inp = "((ad)|bc)*"
    #    inp = "aaa|aaa|bb|dd|eee"
    #    inp = "(a|b)*abb"
    #    inp = "(0|1)*(000|111)(0|1)*"
        nfa = build_nfa(inp)
        dfa = nfa_to_dfa(nfa)
        min_dfa = minimize_dfa(dfa)
        result = match(min_dfa, string)
        f = 10
#        self.assertTrue(result)

    def test_match_False(self):
        input_dfa = self.create_dfa()
        try:
            draw_nfa(input_dfa, "build/input_dfa")
        except Exception as err:
            msg = str(err)
        string = "00"
        result = match(input_dfa, string)

        self.assertFalse(result)
