#!/usr/bin/env python3

from copy import deepcopy
from nfa import *
from collections import deque


class NfaToDfaConverter:
    def __init__(self, nfa):
        self.nfa: Nfa = nfa
        self.clear()

    def clear(self):
        self.count = 0
        self.dfa = Nfa()
        self.dfa_to_nfa_st = {}
        self.dfa_marked_st = {}

    def convert(self):
        self.clear()

        self.dfa.alphabet = self.nfa.alphabet

        eps_clsr = self.eps_closure({self.nfa.start, })
        self.dfa.start = self.add_new_st_to_dfa(eps_clsr)
        unmarked = self.find_unmarked()

        while len(unmarked) > 0:
            t = unmarked[0]
            self.mark(t)
            for symb in self.nfa.alphabet:
                mv = self.move(t, symb)
                u = self.eps_closure(mv)
                found_st = self.find_st(u)
                if not found_st:
                    found_st = self.add_new_st_to_dfa(u)
                self.dfa.add_trans(Trans(t, found_st, symb))
            unmarked = self.find_unmarked()

        self.find_finish_sts()
        #        dfa = self.construct_dfa()
        return self.dfa

    def find_finish_sts(self):
        nfa_finish = self.nfa.finish
        for st in nfa_finish:
            for (k, v) in self.dfa_to_nfa_st.items():
                if st in v:
                    self.dfa.add_to_finish(k)

    def find_st(self, eps_set):
        for (k, v) in self.dfa_to_nfa_st.items():
            if eps_set.issubset(v):
                return k
        return None

    def create_dfa_state(self):
        self.count += 1
        msg = "d-{}".format(self.count)
        return State(msg, msg)

    def add_new_st_to_dfa(self, nfa_states, marker=False):
        dfa_state = self.create_dfa_state()
        self.dfa_to_nfa_st[dfa_state.id] = nfa_states
        self.dfa_marked_st[dfa_state.id] = marker

        self.dfa.add_state(dfa_state)
        return dfa_state.id

    def move(self, dfa_state, symb):
        nfa_st_set = self.dfa_to_nfa_st[dfa_state]
        output = set()
        for st in nfa_st_set:
            tr = self.nfa.transitions.get(st, [])
            output |= {t.to_s for t in tr if t.symbol == symb}
        return output

    def find_unmarked(self):
        res = [k for (k, v) in self.dfa_marked_st.items() if not v]
        return res

    def mark(self, st_id, marker=True):
        self.dfa_marked_st[st_id] = marker

    def eps_closure(self, t_set: set):
        stack = [it for it in t_set]
        eps_clsr = deepcopy(t_set)
        while len(stack) > 0:
            t = stack.pop()
            trans = self.nfa.transitions.get(t, [])
            trololo = [tr.to_s for tr in trans if tr.is_empty()]
            for u in trololo:
                if u not in eps_clsr:
                    eps_clsr.add(u)
                    stack.append(u)

        return eps_clsr


class Matcher:
    def __init__(self, input_fa: Nfa):
        self.input_fa = input_fa

    def match(self, string):
        eps_set = self.eps_closure({self.input_fa.start})
        for c in string:
            mv = self.move(eps_set, c)
            eps_set = self.eps_closure(mv)
        inter_set = eps_set.intersection(self.input_fa.finish)

        return True if inter_set else False

    def move(self, set_of_st, symb):
        output = set()
        for st in set_of_st:
            tr = self.input_fa.transitions.get(st, [])
            output |= {t.to_s for t in tr if t.symbol == symb}
        return output

    def eps_closure(self, t_set: set):
        stack = [it for it in t_set]
        eps_clsr = deepcopy(t_set)
        while len(stack) > 0:
            t = stack.pop()
            trans = self.input_fa.transitions.get(t, [])
            trololo = [tr.to_s for tr in trans if tr.is_empty()]
            for u in trololo:
                if u not in eps_clsr:
                    eps_clsr.add(u)
                    stack.append(u)

        return eps_clsr


class DfaMinimizer:
    def __init__(self, dfa: Nfa):
        self.input_dfa = dfa

    def minimize(self):
        invert_trans = self.get_invert_trans()
        reachable = self.dfs(self.input_dfa.start)
        marked = self.build_table(invert_trans)
        component = {k: -1 for k in self.input_dfa.states}
        components_count = 0
        i_was_here = set()
        for i in self.input_dfa.states:
            i_was_here.add(i)
            if i in reachable and component[i] == -1:
                components_count += 1
                component[i] = components_count
                for j in self.input_dfa.states.keys() - i_was_here:
                    if not marked[i][j]:
                        component[j] = components_count

        dfa = self.build_dfa(component)
        return dfa

    def build_dfa(self, component):
        v = {}
        for key, value in sorted(component.items()):
            v.setdefault(value, []).append(key)
        groups = v.values()

        min_dfa = Nfa()
        min_dfa.alphabet = self.input_dfa.alphabet
        (min_dfa.states, new_to_old) = self.create_states(groups)
        min_dfa.start = [st_id for (st_id, old) in new_to_old.items() if self.input_dfa.start in old][0]

        for k in min_dfa.states:
            old = new_to_old[k]
            old_trans = set()
            for st in old:
                trns = self.input_dfa.transitions.get(st, [])
                old_trans |= set(trns)
            for old_tr in old_trans:
                new_from = [k for (k, v) in new_to_old.items() if old_tr.from_s in v][0]
                new_to = [k for (k, v) in new_to_old.items() if old_tr.to_s in v][0]
                new_tr = Trans(new_from, new_to, old_tr.symbol)
                if not any(it.equals(new_tr) for it in min_dfa.transitions.get(new_from, [])):
                    min_dfa.add_trans(new_tr)

        for k in min_dfa.states:
            old = new_to_old[k]
            for old_f in self.input_dfa.finish:
                if old_f in old:
                    min_dfa.add_to_finish(k)

        return min_dfa

    def create_states(self, groups):
        new_to_old = {}
        new_states = {}
        for v in groups:
            id = ",".join(v)
            st = State(id, id)
            new_states[id] = st
            new_to_old[id] = v
        return new_states, new_to_old

    def build_table(self, invert):
        q = deque()
        marked = {}
        for i in self.input_dfa.states:
            marked[i] = {}
            for j in self.input_dfa.states:
                marked[i][j] = False

        is_terminal = {k: k in self.input_dfa.finish for k in self.input_dfa.states}

        for i in marked:
            for j in marked[i]:
                if not marked[i][j] and is_terminal[i] != is_terminal[j]:
                    marked[i][j] = marked[j][i] = True
                    q.append((i, j))

        while q:
            (u, v) = q.popleft()
            for c in self.input_dfa.alphabet:
                u_trns = [tr.to_s for tr in invert.get(u, []) if tr.symbol == c]
                for r in u_trns:
                    v_trns = [tr.to_s for tr in invert.get(v, []) if tr.symbol == c]
                    for s in v_trns:
                        if not marked[r][s]:
                            marked[r][s] = marked[s][r] = True
                            q.append((r, s))
        return marked

    def dfs(self, start):
        visited, stack = set(), [start]
        while stack:
            vertex = stack.pop()
            if vertex not in visited:
                visited.add(vertex)
                got = set(tr.to_s for tr in self.input_dfa.transitions.get(vertex, []))
                stack.extend(got - visited)

        return visited

    def get_invert_trans(self):
        result = {}
        for (outer_k, v) in self.input_dfa.transitions.items():
            for k in self.input_dfa.transitions.keys():
                lst = [Trans(from_s=k, to_s=outer_k, symbol=tr.symbol) for tr in v if tr.to_s == k]
                if lst:
                    lst.extend(result.get(k, []))
                    result[k] = lst
        return result
