#!/usr/bin/env python3 

import json
import copy
from sys import argv

class SpecialSymbols:
    QUOTE = "'"
    EMPTY = "^" # пустой

class PropertyNames:
    GRAMMAR = "grammar"
    NAME = "-name"
    TERM_SYMBS = "terminalsymbols"
    SPELL = "-spell"    
    TERM = "term"
    NONTERM_SYMBS = "nonterminalsymbols"
    NONTERM = "nonterm"
    PRODUCTIONS = "productions"
    PRODUCTION = "production"
    LHS = "lhs"
    RHS = "rhs"
    SYMBOL = "symbol"
    TYPE = "-type"
    START_SYMB = "startsymbol"

def loadGrammar(filename):
    readLines = None
    with open(filename) as fin:
        readLines = fin.read()
    gotJson = json.loads(readLines)
    return gotJson

def eliminateLeftRecursion(inGrammar):
    """
    Устранение левой рекурсии
    """
    grammar = copy.deepcopy(inGrammar)
    nonterms = inGrammar[PropertyNames.NONTERM_SYMBS]
    for i, item in enumerate(nonterms):
        Ai = item[PropertyNames.NAME]
        for j in range(i):
            Aj = nonterms[j][PropertyNames.NAME]
            productions = grammar[PropertyNames.PRODUCTIONS]
            for index, production in enumerate(productions):
                left = production[PropertyNames.LHS]
                right = production[PropertyNames.RHS][0]
#                print("Ai = %s, Aj = %s, left = %s, right = %s " % (Ai, Aj, left, right))
                if Ai == left and right == Aj:
                    substitute(production, grammar)
#                    print("Ai = %s, Aj = %s, prod = %s" % (Ai, Aj, production))

        ordered = getOrderedProductions(Ai, grammar)
        if ordered:
            eliminateImmediateLeftRecursion(Ai, grammar, ordered)
#        showGrammar(grammar)
    return grammar

def substitute(inpProd, grammar):
    productions = grammar[PropertyNames.PRODUCTIONS]
    productions.remove(inpProd)
    rhs = inpProd[PropertyNames.RHS]
    rhsZero = rhs[0]
    rhsTail = rhs[1:] 
    lhs = inpProd[PropertyNames.LHS]
    for prod in productions[:]:
#        print("lhs =", prod[PropertyNames.LHS], "rhs =", prod[PropertyNames.RHS])
        if prod[PropertyNames.LHS] == rhsZero:
            productions.append({
                PropertyNames.LHS: lhs,
                PropertyNames.RHS: prod[PropertyNames.RHS] + rhsTail
            })
#    print("inpProd=%s\ngrammar=%s" % (inpProd, grammar))
#    showGrammar(grammar)


def getOrderedProductions(Ai, grammar):
    productions = grammar[PropertyNames.PRODUCTIONS]
    alphas = []
    bethas = []
    for prod in productions:
        if Ai == prod[PropertyNames.LHS]:
            rhs = prod[PropertyNames.RHS]            
            if len(rhs) > 0 and rhs[0] == Ai:
                alphas.append(rhs)
            else:
                bethas.append(rhs)
    if len(alphas) > 0:
        for prod in productions[:]:
            if Ai == prod[PropertyNames.LHS]:
                productions.remove(prod)
        return alphas, bethas
    else:
        return None

def eliminateImmediateLeftRecursion(Ai, grammar, ordered):
    """
    Устранение непосредственной левой рекурсии
    """
    newAi = Ai + SpecialSymbols.QUOTE
    grammar[PropertyNames.NONTERM_SYMBS].append({PropertyNames.NAME: newAi})
    alphas = ordered[0]
    bethas = ordered[1]
    
    productions = grammar[PropertyNames.PRODUCTIONS]    
    for betha in bethas:
        productions.append({
            PropertyNames.LHS: Ai,
            PropertyNames.RHS: [*betha, newAi]
        })

    for alpha in alphas:
        head = alpha[1:]

        productions.append({
            PropertyNames.LHS: newAi,
            PropertyNames.RHS: [*head, newAi] 
        })
    productions.append({
        PropertyNames.LHS: newAi,
        PropertyNames.RHS: [SpecialSymbols.EMPTY] 
    })



def showGrammar(grammar, msg=""):
    print("\n", msg)
    print("nonterms:")
    nonterms = grammar[PropertyNames.NONTERM_SYMBS]
    for it in nonterms:
        print(it[PropertyNames.NAME], end=" ")
    print()

    print("terms: ")
    terms = grammar[PropertyNames.TERM_SYMBS]
    for it in terms:
        print(it[PropertyNames.SPELL], end=" ")
    print()

    print("Productions")

    for prod in grammar[PropertyNames.PRODUCTIONS]:
        lhs = prod[PropertyNames.LHS]
        print(lhs, end=" -> ")
        for symbol in prod[PropertyNames.RHS]:
            res = find(lambda it : it[PropertyNames.NAME] == symbol, terms)            
            if res:
                print(res[PropertyNames.SPELL], end=" ")            
            else:
                print(symbol, end=" ")            
        print()
    print()

def find(pred, lst):
    for it in lst:
        if pred(it):
            return it
    return None

def eliminateSymbols(inGrammar):
    """
    Устранение недостижимых символов
    """
    grammar = copy.deepcopy(inGrammar)

    prevV = set()
    curV = {grammar[PropertyNames.START_SYMB]}

    while prevV != curV:
        prevV = curV.copy()
        for prod in grammar[PropertyNames.PRODUCTIONS]:
            lhs = prod[PropertyNames.LHS]
            rhs = prod[PropertyNames.RHS]
            shouldStop = False
            for symb in rhs:
                if lhs in prevV and symb not in prevV:
                    curV.add(symb)
                    shouldStop = True
                    break
            if shouldStop:
                break

    nonterms = grammar[PropertyNames.NONTERM_SYMBS]
    newN = curV & {it[PropertyNames.NAME] for it in nonterms}
    for nonterm in nonterms[:]:
        if nonterm[PropertyNames.NAME] not in newN:
            nonterms.remove(nonterm)

    terms = grammar[PropertyNames.TERM_SYMBS]
    newT = curV & {it[PropertyNames.NAME] for it in terms}
    for term in terms[:]:
        if term[PropertyNames.NAME] not in newT:
            terms.remove(term)

    productions = grammar[PropertyNames.PRODUCTIONS]
    for prod in productions[:]:
        lhs = prod[PropertyNames.LHS]
        rhs = prod[PropertyNames.RHS]
        if lhs not in curV:
            productions.remove(prod)
        else:
            for it in rhs[:]:
                if it not in curV:
                    productions.remove(prod)
                    break
    return grammar


if __name__ == "__main__":
    filename = argv[1] if len(argv) >= 2 else "grammar.json"
    grammar = loadGrammar(filename)
    showGrammar(grammar, msg="Начальная грамматика:")
    grammarOne = eliminateLeftRecursion(grammar)
    showGrammar(grammarOne, msg="Грамматика после устранения левой рекурсии:")
    grammarTwo = eliminateSymbols(grammar)
    showGrammar(grammarTwo, msg="Грамматика после устранения недостижимых символов:")