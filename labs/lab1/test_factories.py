class EliminateLeftRecursionInputGrammarsFactory:
    def createSimplestGrammar(self):
        return {
            "-name": "G0",
            "terminalsymbols": [
                {
                    "-name": "IDENT",
                    "-spell": "a"
                },
                {
                    "-name": "ADD",
                    "-spell": "+"
                },
                {
                    "-name": "MUL",
                    "-spell": "*"
                },
                {
                    "-name": "LPAREN",
                    "-spell": "("
                },
                {
                    "-name": "RPAREN",
                    "-spell": ")"
                }
            ],
            "nonterminalsymbols": [
                {
                    "-name": "E"
                },
                {
                    "-name": "T"
                },
                {
                    "-name": "F"
                }
            ],
            "productions": [
                {
                    "lhs": "E",
                    "rhs": ["E", "ADD", "T"]
                },
                {
                    "lhs": "E",
                    "rhs": ["T"]
                },
                {
                    "lhs": "T",
                    "rhs": ["T", "MUL", "F"]
                },
                {
                    "lhs": "T",
                    "rhs": ["F"]
                },
                {
                    "lhs": "F",
                    "rhs": ["IDENT"]
                },
                {
                    "lhs": "F",
                    "rhs": ["LPAREN", "E", "RPAREN"]
                }
            ],
            "startsymbol": "E"
        }


class EliminateLeftRecursionOutputGrammarsFactory:
    def createSimplestGrammar(self):
        return {'terminalsymbols': [{'-spell': 'a', '-name': 'IDENT'}, {'-spell': '+', '-name': 'ADD'},
                                    {'-spell': '*', '-name': 'MUL'}, {'-spell': '(', '-name': 'LPAREN'},
                                    {'-spell': ')', '-name': 'RPAREN'}],
                'nonterminalsymbols': [{'-name': 'E'}, {'-name': 'T'}, {'-name': 'F'}, {'-name': "E'"},
                                       {'-name': "T'"}],
                'productions': [{'rhs': ['IDENT'], 'lhs': 'F'}, {'rhs': ['LPAREN', 'E', 'RPAREN'], 'lhs': 'F'},
                                {'rhs': ['T', "E'"], 'lhs': 'E'}, {'rhs': ['ADD', 'T', "E'"], 'lhs': "E'"},
                                {'rhs': ['^'], 'lhs': "E'"}, {'rhs': ['F', "T'"], 'lhs': 'T'},
                                {'rhs': ['MUL', 'F', "T'"], 'lhs': "T'"}, {'rhs': ['^'], 'lhs': "T'"}],
                'startsymbol': 'E', '-name': 'G0'}


class EliminateSymbolsInputGrammarsFactory:
    def createSimplestGrammar(self):
        return {
            "-name": "G0",
            "terminalsymbols": [
                {
                    "-name": "ADD",
                    "-spell": "+"
                },
                {
                    "-name": "MUL",
                    "-spell": "*"
                }
            ],
            "nonterminalsymbols": [
                {
                    "-name": "E"
                },
                {
                    "-name": "T"
                }
            ],
            "productions": [
                {
                    "lhs": "E",
                    "rhs": ["ADD"]
                },
                {
                    "lhs": "T",
                    "rhs": ["MUL"]
                }
            ],
            "startsymbol": "E"
        }


class EliminateSymbolsOutputGrammarsFactory:
    def createSimplestGrammar(self):
        return {
            "-name": "G0",
            "terminalsymbols": [
                {
                    "-name": "ADD",
                    "-spell": "+"
                }
            ],
            "nonterminalsymbols": [
                {
                    "-name": "E"
                }
            ],
            "productions": [
                {
                    "lhs": "E",
                    "rhs": ["ADD"]
                }
            ],
            "startsymbol": "E"
        }
