#!/usr/bin/env python3 

import unittest
from lab1 import eliminateLeftRecursion, PropertyNames, eliminateSymbols
from test_factories import *


class EliminateLefRecursionTest(unittest.TestCase):
    def setUp(self):
        self.inputFactory = EliminateLeftRecursionInputGrammarsFactory()
        self.outputFactory = EliminateLeftRecursionOutputGrammarsFactory()

    def test_simplest_removesLeftRecursion(self):
        inp = self.inputFactory.createSimplestGrammar()
        expected = self.outputFactory.createSimplestGrammar()
        actual = eliminateLeftRecursion(inp)
        self.assertEqual(expected, actual)

    def create_simple_grammar(self):
        return {
            PropertyNames.NAME: "test"
        }


class EliminateSymbolsTest(unittest.TestCase):

    def setUp(self):
        self.inputFactory = EliminateSymbolsInputGrammarsFactory()
        self.outputFactory = EliminateSymbolsOutputGrammarsFactory()

    def test_simplest_eliminateSymbs(self):
        inp = self.inputFactory.createSimplestGrammar()
        expected = self.outputFactory.createSimplestGrammar()
        actual = eliminateSymbols(inp)
        self.assertEqual(expected, actual)

if __name__ == "__main__":
    unittest.main()
