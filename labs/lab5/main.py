#!/usr/bin/env python3

class Consts:
    MARKER = "$"
    BLANK = " "
    MAX = 1000
    MAX_CH = 256

class Terminals:
    ATOM = 0
    NOT = 1
    AND = 2
    OR = 3
    IMP = 4
    EQU = 5
    LPAR = 6
    RPAR = 7
    DOLL = 8

class ErrorCode:
    NO_OPERAND = "1"
    RPAR_NOT_BAL = "2"
    NO_OPERATOR = "3"
    NO_RPAR = "4"

class Predsh:
    MORE = ">"
    LESS = "<"
    EQU = "="

def end_char(matrix, symbols, s, t):
    s_t = ord(s[t])
    i = symbols[s_t]
    s_t_one = ord(s[t+1])
    j = symbols[s_t_one]
    res = matrix[i][j]
    return res

def get_error_msg(error_msg, matrix, symbols, s, t, ch):
    s_t = ord(s[t])
    i = symbols[s_t]
    code = ord(ch)
    j = symbols[code]
    matr_it = matrix[i][j]
    return error_msg[matr_it]

def gen_prn(c, prn: list):
    if c not in {"(", ")"}:
       prn.append(c)

if __name__ == "__main__":
    error_msg = {
        ErrorCode.NO_OPERAND: "отсутствует операнд",
        ErrorCode.RPAR_NOT_BAL: "несбалансированная правая скобка",
        ErrorCode.NO_OPERATOR: "отсутствует оператор",
        ErrorCode.NO_RPAR: "отсутствует правая скобка"
    }
    matrix =[
        ['3','>','>','>','>','>','3','>','>'],
        ['<','<','>','>','>','>','<','>','>'],
        ['<','<','>','>','>','>','<','>','>'],
        ['<','<','<','>','>','>','<','>','>'],
        ['<','<','<','<','>','>','<','>','>'],
        ['<','<','<','<','<','>','<','>','>'],
        ['<','<','<','<','<','<','<','=','4'],
        ['3','>','>','>','>','>','3','>','>'],
        ['<','<','<','<','<','<','<','2','1']
    ]

    mappp = {
        "~": Terminals.NOT,
        "&": Terminals.AND,
        "|": Terminals.OR,
        ">": Terminals.IMP,
        "=": Terminals.EQU,
        "(": Terminals.LPAR,
        ")": Terminals.RPAR,
        "$": Terminals.DOLL
    }

    symbols = [-1 for i in range(Consts.MAX_CH)]
    for i in range(Consts.MAX_CH):
        ch = chr(i)
        if ch.isalpha() or ch in ['0', '1']:
            symbols[i] = Terminals.ATOM
        elif ch in mappp:
            symbols[i] = mappp[ch]
        else:
            symbols[i] = Terminals.DOLL
    prn = []
    done = False
    flag = False
    marker = "$"
    string = iter(input("Введите инфиксное выражение (в конце '$'; $ - выход) >"))
    s = [-1  for i in range(1000)]
    shouldContinue = True
    while shouldContinue:
        ch = next(string, marker)
        if (ch == marker):
            break

        s[0] = marker
        t = 0
        while (t > 0) or (ch != marker):
            s_t = ord(s[t])
            i = symbols[s_t]
            ord_ch = ord(ch)
            j = symbols[ord_ch]
            matr_it = matrix[i][j]
            if matr_it in ["<", "="]:
                t += 1
                s[t] = ch
                ch = next(string, marker)
            elif matr_it == ">":
                while  True:
                    s_it = s[t]
                    gen_prn(s_it, prn)
                    t -= 1
                    if end_char(matrix, symbols, s, t) == "<":
                        break
            else:
                msg = get_error_msg(error_msg, matrix, symbols, s, t, ch)
                print("ОШИБКА: {}".format(msg))
                flag = True
        if flag:
            flag = False
        else:
            print("Постфиксная запись")
            for value in prn:
                print(value, end="")
            print()
