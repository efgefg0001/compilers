#!/usr/bin/env python3

"""
Васюнин А. Н., группа ИУ7-27
Вариант - 9
Запуск: python3 rk_1.py input.txt output.txt
"""

from sys import argv
import math
import copy


class Symbols:
    PROCENT = "%"
    EQ = "="
    EMPTY = ""


class NumConsts:
    MIN = 0
    MAX = 255


class DataSet:
    def __init__(self):
        self.numOfSamples = 0
        self.input = {}
        self.output = {}


class Expr:
    def __init__(self, left=Symbols.EMPTY, right=Symbols.EMPTY, order=0):
        self.right = right
        self.left = left
        self.order = order


class Solver:
    def __init__(self, expr):
        self.expr = expr

    def eval(self, args):
        return eval(self.expr, args)


def readFromFile(filename):
    with open(filename) as fin:
        numOfDataSets = int(fin.readline())
        dataSetList = []
        for i in range(numOfDataSets):
            numOfStreams = int(fin.readline())
            dataSet = DataSet()
            dataSet.numOfSamples = int(fin.readline())
            for j in range(numOfStreams):
                curStream = fin.readline().strip()
                if Symbols.PROCENT in curStream:
                    left, rawRight = [it.strip() for it in curStream.split(Symbols.PROCENT)]
                    right = [float(it) for it in rawRight.split()]
                    dataSet.input[left] = Expr(left, right, j)
                elif Symbols.EQ in curStream:
                    left, right = [it.strip() for it in curStream.split(Symbols.EQ)]
                    dataSet.output[left] = Expr(left, right, j)
            dataSetList.append(dataSet)

    return dataSetList


class MagicList:
    def __init__(self, samples, cur=0):
        self.samples = samples
        self.cur = cur

    def __getitem__(self, index):
        realIndex = self.cur + index
        if realIndex < 0:
            return 0
        elif realIndex >= len(self.samples):
            return 0
        else:
            return self.samples[realIndex]


def findResultDataSets(inputDataSets: list):
    result = []
    for dataSet in inputDataSets:
        used = set()
        output = DataSet()
        output.numOfSamples = dataSet.numOfSamples

        exprForEval, magicDict = findExprWithOnlyInputRightPart(dataSet, used)

        while exprForEval:
            used.add(exprForEval.left)
            outExpr = Expr(left=exprForEval.left, right=[], order=exprForEval.order)
            for index in range(output.numOfSamples):
                for key in magicDict.keys():
                    magicDict[key].cur = index
                val = compAndRound(exprForEval.right, copy.deepcopy(magicDict))
                outExpr.right.append(val)
            output.output[outExpr.left] = outExpr
            exprForEval, magicDict = findExprWithOnlyInputRightPart(dataSet, used)

        items = {it for it in dataSet.output.keys() if it not in used}
        while items:
            ok, exprForEval, magicDict = findDataForTransitive(items, used, dataSet, output)
            if ok:
                used.add(exprForEval.left)
                outExpr = Expr(left=exprForEval.left, right=[], order=exprForEval.order)
                for index in range(output.numOfSamples):
                    for key in magicDict.keys():
                        magicDict[key].cur = index
                    val = compAndRound(exprForEval.right, copy.deepcopy(magicDict))
                    outExpr.right.append(val)
                items.remove(outExpr.left)
                output.output[outExpr.left] = outExpr
            #        for expr in dataSet.output:
            #            if expr.left not in used:
            # множество пока не пусто выполнять подсчёт

        # find only with input
        # find another

        result.append(output)

    return result


def findDataForTransitive(items, used, inpDataSet: DataSet, outDataSet: DataSet):
    for it in items:
        expr = inpDataSet.output[it]
        magicDict = {}
        for k, v in inpDataSet.input.items():
            if k in expr.right:
                magicDict[k] = MagicList(v.right)
        ok = False
        for k, v in inpDataSet.output.items():
            if k in expr.right:
                if k in outDataSet.output.keys():
                    ok = True
                    computed = outDataSet.output[k].right
                    magicDict[k] = MagicList(computed)
                    return ok, expr, magicDict
        return ok, expr, magicDict


def findExprWithOnlyInputRightPart(dataSet: DataSet, used: set) -> Expr:
    magicDict = {}
    for key, expr in dataSet.output.items():
        for k, v in dataSet.input.items():
            if k in expr.right:
                magicDict[k] = MagicList(v.right)
        condB = any(it in expr.right for it in dataSet.output.keys())
        condC = expr.left not in used
        if not condB and condC:
            return expr, magicDict
    return None, None


def compVal(expr, args):
    return Solver(expr).eval(args)


def compAndRound(expr, args):
    val = compVal(expr, args)
    if val < NumConsts.MIN:
        return NumConsts.MIN
    elif val > NumConsts.MAX:
        return NumConsts.MAX
    else:
        return math.floor(val)


def writeToFile(resultDataSets, filename):
    with open(filename, "w") as fout:
        for i, dataSet in enumerate(resultDataSets):
            fout.write("DATA SET #{}\n".format(i + 1))
            vls = list(dataSet.output.values())
            srtd = sorted(vls, key=lambda it: it.order)
            for it in srtd:
                outRight = " ".join([str(val) for val in it.right])
                fout.write("{} % {}\n".format(it.left, outRight))


if __name__ == "__main__":
    #    value = comp_val("5*x + 1 * y", {"x": 10, "y": 20})
    try:
        if len(argv) < 3:
            raise Exception("Omitted input filename or output filename")
        inputDataSetList = readFromFile(argv[1])
        resultDataSets = findResultDataSets(inputDataSetList)
        writeToFile(resultDataSets, argv[2])
    except Exception as error:
        print("Error:", error)
